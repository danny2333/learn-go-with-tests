package main

import (
	"bytes"
	"fmt"
	"testing"
)

func Greet(writer *bytes.Buffer, name string) {
	fmt.Fprintf(writer, "Hello, %s", name)
}

func TestGreet(t *testing.T) {
	buffer := bytes.Buffer{}
	Greet(&buffer, "Danny")

	got := buffer.String()
	want := "Hello, Danny"

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
