package main

import (
	"fmt"
	"strings"
	"testing"
)

// MyString string
type MyString string

// Rect Struct
type Rects struct {
	width  float64
	heigth float64
}

func explain(i interface{}) {
	// fmt.Printf("value given to explain function is of type '%T' width value %v\n", i, i)
	switch i.(type) {
	case string:
		fmt.Println("i stored string", strings.ToUpper(i.(string)))
	case int:
		fmt.Println("i stored int", i)
	default:
		fmt.Println("i stored something else", i)
	}
}

func TestMain(t *testing.T) {
	ms := MyString("Hello World!")
	r := Rects{5.5, 4.5}
	explain(ms) // value given to explain function is of type 'main.MyString' width Hello World!
	explain(r)  // value given to explain function is of type 'main.Rects' width {5.5, 4.5}
}
