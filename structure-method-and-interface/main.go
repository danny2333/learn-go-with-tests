package main

import (
	"fmt"
	"math"
)

// Shape Interface
type Shape interface {
	Area() float64
	Perimeter() float64
}

// Rect Struct
type Rect struct {
	width  float64
	heigth float64
}

// Circle Struct
type Circle struct {
	radius float64
}

// Area 宽高声明方法
func (r Rect) Area() float64 {
	return r.width * r.heigth
}

// Perimeter 周长声明方法
func (r Rect) Perimeter() float64 {
	return 2 * (r.width + r.heigth)
}

// Area 宽高声明方法2
func (c Circle) Area() float64 {
	return math.Pi * c.radius * c.radius
}

// Perimeter 周长声明方法2
func (c Circle) Perimeter() float64 {
	return 2 * math.Pi * c.radius
}

func main() {
	var s Shape
	s = Rect{5.0, 4.0}
	r := Rect{5.0, 3.0}
	fmt.Printf("type of s is %T\n", s)
	fmt.Printf("value of s is %v\n", s)
	fmt.Println("area of rectrange s", s.Area())
	fmt.Println("perimeter of rectrange s", r.Perimeter())
	fmt.Println("s == r is", s == r)

	s = Circle{10}
	fmt.Printf("type of s is %T\n", s)
	fmt.Printf("value of s is %v\n", s)
	fmt.Printf("value of s is %0.2f\n", s.Area())
}
