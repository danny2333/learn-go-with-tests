package main

import "testing"

func TestHello(t *testing.T) {
	//将断言重构为一个函数，减少重复并提高测试的可读性
	assertCorrectMessage := func(t *testing.T, got, want string) {
		//表明这个方法是辅助函数，通过这样，当测试失败时所报告的行号将在函数调用中而不是
		//在辅助函数内部。这样容易跟踪问题
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}
	//子测试
	t.Run("saying hello to people", func(t *testing.T) {
		got := Hello("danny")
		want := "Hello, danny"
		/* if got != want {
			t.Errorf("got %q want %q", got, want)
		} */
		assertCorrectMessage(t, got, want)
	})

	t.Run("empty string defaults to 'World'", func(t *testing.T) {
		got := Hello("")
		want := "Hello, World"
		/* if got != want {
			t.Errorf("got %q want %q", got, want)
		} */
		assertCorrectMessage(t, got, want)
	})
}
