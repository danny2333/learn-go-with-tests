package iteration

import "testing"

const repeatCount = 5

func Repeat(character string) string {
	var repeated string
	for i := 0; i < repeatCount; i++ {
		repeated += character
	}
	return repeated
}

// 基准测试 代码运行b.N次，并测量需要多长时间
// 用go test -bench=. 来运行基准测试，如果在Windows Powershell环境使用 go test -bench="."
// 12222 xxx ns/op 意思时我们的函数在测试时平均运行时间为xxx纳秒，为了测试这个，它运行了12222次
func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a")
	}
}

/* func TestRepeat(t *testing.T) {
	repeated := Repeat("a")
	expected := "aaaa"

	if repeated != expected {
		t.Errorf("expected %q but got %q", expected, repeated)
	}
}
*/
